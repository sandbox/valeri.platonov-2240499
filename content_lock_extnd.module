<?php

include_once 'content_lock_extnd.common.inc';
include_once 'content_lock_extnd.admin.inc';
include_once 'includes/content_lock_extnd.views_alter.inc';

/**
 * Implementation of hook_menu().
 */
function content_lock_extnd_menu() {
  $items['admin/config/content/content_lock_extnd'] = array (
    'type' => MENU_NORMAL_ITEM,
    'title' => 'Content lock Extended',
    'description' => 'Configuration options for the Content lock Extended module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('content_lock_extnd_admin_settings_exclude'),
    'access arguments' => array('administer site configuration'),
    'file' => 'content_lock_extnd.admin.inc'
   );
  $items['admin/content/content_lock/release/%/%'] = array(
    'page callback' => 'content_lock_extnd_release_item',
    'page arguments' => array(4, 5, NULL),
    'access arguments' => array('administer checked out documents'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/content/%/%/content_lock/releaseown'] = array(
    'page callback' => 'content_lock_extnd_release_own_item',
    'page arguments' => array(2, 3, TRUE, FALSE),
    'access arguments' => array('check out documents'),
    'type' => MENU_CALLBACK,
  );
  $items['ajax/content_lock/%/%/canceledit'] = array (
    'page callback' => 'content_lock_extnd_release_own_item',
    'page arguments' => array(2, 3, FALSE, FALSE),
    'access callback' => true,
  );
  return $items;
}

/**
 * Implementation of hook_menu_alter().
 */
function content_lock_extnd_menu_alter(&$items) {
	// override standart path to
	$items['admin/content/content_lock'] = array(
    'title' => 'Extended Locked documents',
    'page callback' => 'content_lock_overview',
  );
	$items['user/%user/content_lock'] = array(
    'title' => 'Extended Locked documents',
    'page callback' => 'content_lock_overview',
    'page arguments' => array(1),
  );
}

/**
 * alter content_lock_admin form for extended settings
 */
function content_lock_extnd_form_content_lock_admin_settings_alter(&$form, &$form_state) {
		unset($form['content_lock_allowed_node_types']);
		content_lock_extnd_admin_settings($form);
}

/**
 * hook_entity_info_alter
 * @param unknown $entity_info
 */
function content_lock_extnd_entity_info_alter(&$entity_info) {
	$entity_support = array_filter(variable_get('content_lock_extnd_entity_support', array()));
	foreach($entity_info as $entity_type => $info) {
		if ($entity_type == 'node') continue;
		if (in_array($entity_type, $entity_support) && !isset($entity_info[$entity_type]['content_lock_extnd']['validation callback'])) {
			$entity_info[$entity_type]['content_lock_extnd']['validation callback'] = 'content_lock_extnd_entity_validate';
		}
	}
}

/**
 * @param QueryAlterableInterface $query
 */
function content_lock_extnd_query_alter(QueryAlterableInterface &$query) {
	$tables =& $query->getTables();
	$where =& $query->conditions();
	// we need to replace all select delete op on content_lock table for content_lock module
	if(in_array('content_lock', $tables) && in_array('nid', $where) && !in_array('entity_type', $where)) {
		$query->condition('entity_type', 'node');
	}
}

/**
 * Implement hook_entity_validate() to check that the user is
 * maintaining his lock.
 *
 * @param $$entity
 * @param $entity_type
 *   The entity whose lock to validate.
 * @param $form
 *   The entitie's form.
 * @param $form_state
 *   Form state.
 */
function content_lock_extnd_entity_validate($entity, $entity_type, $form, &$form_state) {
  global $user;

  	$entity_type = $form['#entity_type'];
	  if ($entity_type == 'node') return;
	  $entity_info = entity_get_info($entity_type);
	  $entity_id = $entity->{$entity_info['entity keys']['id']};
	  $destination = str_replace( '%' . $entity_type , $entity_id, $entity_info['default path']);

  if (isset($entity_id) && _content_lock_extnd_lockable_entity($entity_type, $form['#bundle']) && user_access('check out documents')) {
    // Existing entity. Check if we still own the lock.
    if ($lock = content_lock_extnd_fetch_lock($entity_type, $entity_id)) {
      if ($lock->uid != $user->uid) {
        // Lock is no longer ours.
        form_set_error('changed', t('Your lock has been removed!') .'<br />'. content_lock_lock_owner($lock) .'<br />'. t('You can still save the content if this user aborts the edit operation without saving changes.'));
      }
    }
    else {
      // Entity is not locked. Try to re-lock if entity is unchanged.
      if (!content_lock_extnd_entity($entity_type, $entity_id, $user->uid)) {
        form_set_error('alsochanged', t('Your lock has been removed due to inactivity or by an administrator. Failed to regain the lock since the document has been changed since. Please !discard.',
            array('!discard' => l('discard your changes', $destination))));
      }
    }
  }
}

/**
 * Implement hook_entity_update() to unlock a entity after it's saved.
 *
 * This hook is invoked after hook_entity_save() is invoked and after
 * the changes to the entity are actually saved to the database, so it's
 * the right place to unlock a modified entity.
 *
 * @param (object) $entity
 * @param (string) $entity_type
 */
function content_lock_extnd_entity_update($entity, $entity_type) {
  global $user;
  if (empty($entity) || $entity_type == 'node') return;
  $entity_info = entity_get_info($entity_type);
	$entity_id = $entity->{$entity_info['entity keys']['id']};
	// check !empty($entity_info['entity keys']['bundle']) to clearing cache
  if (!empty($entity_info['entity keys']['bundle']) && _content_lock_extnd_lockable_entity($entity_type, $entity->{$entity_info['entity keys']['bundle']})) {
    content_lock_extnd_release($entity_type, $entity_id, $user->uid);
  }
}

/**
 * Implement hook_entity_delete() to remove references to deleted entities
 * from the lock tables.
 *
 * @param (object) $entity
 * @param (string) $entity_type
 */
function content_lock_extnd_entity_delete($entity, $entity_type) {
	if ($entity_type == 'node') return;
	$entity_info = entity_get_info($entity_type);
	$entity_id = $entity->{$entity_info['entity keys']['id']};
  content_lock_extnd_release($entity_type, $entity_id, NULL);
}

/**
 * Implementation of hook_form_alter().
 */
function content_lock_extnd_form_alter(&$form, &$form_state, $form_id) {
	global $user;
	$entity = empty($form['#entity']) ? NULL : $form['#entity'];
	if($entity) {
	  $entity_type = $form['#entity_type'];
	  if ($entity_type == 'node') return;
	  $bundle = $form['#bundle'];
	  $entity_info = entity_get_info($entity_type);
	  $entity_controller = entity_get_controller($entity_type);
	  $entity_id = $entity->{$entity_info['entity keys']['id']};
		$destination = str_replace( '%' . $entity_type , $entity_id, $entity_info['default path']);
		$skip_lock = FALSE;
    $result = module_invoke_all('content_lock_extnd_skip_locking', $entity_id, $form_id, $form, $form_state);
    foreach($result as $bool) {
      if (is_bool($bool)) {
        $skip_lock = $skip_lock || $bool;
      }
    }
    if ($skip_lock == FALSE) {
    	// if we should lock or already have been locked, load the unload js
      if(variable_get('content_lock_unload_js', true)) {
        $form['#after_build'][] = '_content_lock_extnd_add_unload_js';
      }
      // Adding cancel button, if configured
      if(variable_get('content_lock_admin_cancelbutton', true)) {
        _content_lock_extnd_add_cancelbutton($form, $form_state, $form_id);
      }
      if (is_object($form['#entity']) && is_numeric($entity_id)) {
      	if(!empty($form_state['rebuild']) && $form_state['rebuild'] == TRUE) {
          // We dont need anything here right now
        }
        // If the form did not get submitted we show it the first time
        // so try to get the lock if possible
        else if ($form_state['submitted'] === FALSE) {
          // Finally set the lock if everthing passed.
          if(content_lock_extnd_entity($entity_type, $entity_id, $user->uid) == false) {
            // could not lock node, it's locked by someone else
            drupal_goto($destination);
          }
        }
        // form['#validate'][] = 'content_lock_extnd_entity_validate';
      }
    }
 	}
}

/**
 * replace for ajax : 'nid' => $entity_type . '/' . $entity_id
 */
function _content_lock_extnd_add_unload_js(&$form, $form_state) {
  $m = drupal_get_path('module','content_lock');
  drupal_add_js("$m/js/jquery.url.packed.js", array('group' => JS_LIBRARY));
  drupal_add_js("$m/js/onUserExit.js", array('group' => JS_LIBRARY));
  drupal_add_js("$m/js/content_lock_init.js");

	$entity = empty($form['#entity']) ? NULL : $form['#entity'];
 	if($entity) {
    $entity_type = $form['#entity_type'];
	  $bundle = $form['#bundle'];
	  $entity_info = entity_get_info($entity_type);
	  $entity_id = $entity->{$entity_info['entity keys']['id']};
    $destination = str_replace( '%' . $entity_type , $entity_id, $entity_info['default path']);

    $internal_urls = array();
    $internal_form_selectors = array();

    $internal_urls[] = $destination . '/edit';
    $internal_form_selectors[] = 'form.node-form';
    $lock = content_lock_extnd_fetch_lock($entity_type , $entity_id);
    $token = content_lock_extnd_get_release_token($entity_type , $entity_id);
    $settings = array(
      'nid' => $entity_type . '/' . $entity_id,
      'ajax_key' => $lock->ajax_key,
  	  'token' => $token,
 		  'unload_js_message_enable' => variable_get('content_lock_unload_js_message_enable', TRUE),
      'internal_urls' => implode('|', $internal_urls),
      'internal_forms' => implode(', ', $internal_form_selectors),
    );
    if ($settings['unload_js_message_enable']) {
      $settings['unload_js_message'] = variable_get('content_lock_unload_js_message', 'If you proceed, ALL of your changes will be lost.');
    }

    $called =& drupal_static(__FUNCTION__ . '__called');
    if (!empty($called)) {
      $called++;
      return $form;
    }
    $called = 1;
    // replace settings for js code
    drupal_add_js(array('content_lock' => $settings), 'setting');
 	}
  return $form;
}

/**
 * add entity support
 * @param unknown $form
 * @param unknown $form_state
 * @param unknown $form_id
 */
function _content_lock_extnd_add_cancelbutton(&$form, $form_state, $form_id) {
  // If we're on the node form
  if($form['#node']) return;
  $entity = empty($form['#entity']) ? NULL : $form['#entity'];
	if($entity) {
    $entity_info = entity_get_info($form['#entity_type']);
	  $entity_id = $entity->{$entity_info['entity keys']['id']};
	}
  if ($entity_id) {
      $form['actions']['cancel'] = array(
        '#type' => 'button',
        '#weight' => 2000,
        '#value' => t('Cancel'),
        '#validate' => array('content_lock_extnd_cancel_submit'),
      );
      if (isset($form['actions']['delete'])) {
        $form['actions']['delete']['#weight'] = 2001;
      }
  }
}

/**
 *  Implementation of our own skip_locking api to implement our logic to skip locks
 */
function content_lock_extnd_content_lock_extnd_skip_locking($entity_id, $form_id, $form, $form_state) {
  global $user;
  if (!_content_lock_extnd_lockable_entity($form['#entity_type'], $form['#bundle'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Callback for a cancel request on a form
 * Release the own entity lock
 */
function content_lock_extnd_cancel_submit(&$form, &$form_state) {
	$entity_info = entity_get_info($form['#entity_type']);
  $entity_id = $form['#entity']->{$entity_info['entity keys']['id']};
  content_lock_extnd_release_own_item($form['#entity_type'], $entity_id, TRUE, TRUE);
}

function content_lock_extnd_release_item($entity_type , $entity_id, $account = NULL) {
  global $user;
  if (empty($_GET['token']) || !drupal_valid_token($_GET['token'], "content_lock/release/$entity_type/{$entity_id}")) {
    return MENU_ACCESS_DENIED;
  }
  if (!$account && _content_lock_verbose()) {
    $lock = content_lock_extnd_fetch_lock($entity_type, $entity_id);
  }
  content_lock_extnd_release($entity_type, $entity_id, $account ? $account->uid : NULL);
  if(_content_lock_verbose()) {
    if (!empty($lock) && !$account && $user->uid != $lock->uid) {
      $lock_account = user_load($lock->uid);
      drupal_set_message(t('The editing lock held by !user has been released.', array('!user' => theme('username', array('account' => $lock_account)))), 'status', FALSE);
    } else {
      drupal_set_message(t('The editing lock has been released.'),'status', FALSE);
    }
  }
  drupal_goto($account ? "user/{$account->uid}/content_lock" : 'admin/content/content_lock');
}

function content_lock_extnd_release_own_item($entity_type , $entity_id, $response = TRUE, $ignore_token = FALSE) {
  global $user;
  if (!$ignore_token) {
    if (!isset($_GET['token']) || !drupal_valid_token($_GET['token'], "content_lock/release/$entity_type/{$entity_id}")) {
      return MENU_ACCESS_DENIED;
    }
  }
  if($entity_id != NULL) {
     // Imply that this is an AJAX request if we aren't expected to
     // interface with a human.
    if (!$response) {
      $lock = content_lock_extnd_fetch_lock($entity_type, $entity_id);
      if (strcmp($_GET['k'], $lock->ajax_key)) {
        /* the key doesn't match, don't unlock the node */
        if ($response) {
          drupal_set_message('Trounced AJAX unlock request.', 'status', FALSE);
        }
        exit();
      }
    }
    content_lock_extnd_release($entity_type, $entity_id, $user->uid);
    if($response) {
    	$entity_info = entity_get_info($entity_type);
		  $destination = str_replace( '%' . $entity_type , $entity_id, $entity_info['default path']);
      drupal_goto($destination);
    }
    else {
      exit();
    }
  }
  else {
    if($response) {
      drupal_goto();
    }
    else {
      exit();
    }
  }
}

/**
 * Build an overview of locked documents.
 * will add views support in next release
 * @param $account
 *   A user object.
 */
function content_lock_extnd_overview($account = NULL) {
  global $user;
}

/**
 * Try to lock a document for editing.
 */
function content_lock_extnd_entity($entity_type, $entity_id, $uid, $quiet = FALSE) {
  $lock = content_lock_extnd_fetch_lock($entity_type, $entity_id);

  if ($lock != FALSE && $lock->uid != $uid) {
    $message = content_lock_lock_owner($lock);
    if (user_access('administer checked out documents')) {
      $url = "admin/content/content_lock/release/$entity_type/$entity_id";
    }

    if (isset($url)) {
      $token = content_lock_extnd_get_release_token($entity_type, $entity_id);
      $message .= '<br />'. t('Click !here to check back in now.', array('!here' => l(t('here'), $url, array('query' => array('token' => $token, 'destination' => $_GET['q'])))));
    }
    if(!empty($message)) {
      drupal_set_message($message, 'warning', FALSE);
    }
    return FALSE;
  }
  else {
    // no lock yet, create one
    if($lock == false) {
    	$bundle = NULL;
    	$entities = entity_load($entity_type, array($entity_id));
    	$entity_body = $entities[$entity_id];
    	$entity_info = entity_get_info($entity_type);
    	$bundle_key = $entity_info['entity keys']['bundle'];
      $bundle = $entity_body->{$bundle_key};
      // Lock node.
      $data = array(
      	'entity_type' => $entity_type,
      	'bundle' => $bundle,
        'nid' => $entity_id ,
        'uid' => $uid,
        'timestamp' => time(),
        'ajax_key' => rand(),
      );
      // compatibility for future schema alteration in releases
      drupal_write_record('content_lock',$data);

      if(_content_lock_verbose() && !$quiet) {
        drupal_set_message(t('This document is now locked against simultaneous editing. It will unlock when you navigate elsewhere.'), 'status', FALSE);
      }
      module_invoke_all('content_lock_extnd_locked', $entity_type, $entity_id, $uid);
    } else {
      /* A lock already exists: update its AJAX key */
      $lock->ajax_key = rand();
      // use primary key for entity : if (!drupal_write_record('content_lock', $lock, array('entity_type', 'entity_id'))) {
      if (!drupal_write_record('content_lock', $lock, array('entity_type', 'nid'))) {
        drupal_write_record('content_lock', $lock);
      }
    }
  }
  return TRUE;
}

/**
 * Calculate the token required to unlock a node.
 *
 * Tokens are required because they prevent CSRF,
 * https://security.drupal.org/node/2429.
 */
function content_lock_extnd_get_release_token($entity_type, $entity_id) {
  return drupal_get_token("content_lock/release/$entity_type/{$entity_id}");
}

/**
 *
 * @param unknown $uid
 * @param unknown $entity
 * @return boolean
 */
function _content_lock_extnd_still_locked($uid, $entity_type, $entity_id) {
  $query = db_select('content_lock', 'c')
        ->fields('c', array('nid'))
        ->condition('entity_type', $entity_type)
        ->condition('nid', $entity_id)
        ->condition('uid', $uid)
        ->countQuery();
  $result = $query->execute();
  return (bool)$result->fetchField();
}

/**
 * Fetch the lock for a entity.
 * @param $entity_type
 * @param $entity_id
 *   an entity id.
 * @return
 *   The lock object for the entity. FALSE, if the document is not locked.
 */
function content_lock_extnd_fetch_lock($entity_type, $entity_id) {
  $query = db_select('content_lock', 'c')
    ->fields('c')
    ->condition('c.entity_type', $entity_type)
    ->condition('c.nid', $entity_id);
  $u = $query->leftJoin('users', 'u', '%alias.uid = c.uid');
  $query->fields($u, array('name'));
  return $query->execute()->fetchObject();
}

/**
 * Check whether a entity is configured to be protected by content_lock_extnd.
 */
function _content_lock_extnd_lockable_entity($entity_type, $bundle) {
  return in_array($bundle, content_lock_extnd_content_lock_extnd_entity_lockable($entity_type));
}

/**
 * Release a locked entity.
 *
 * @param $eid
 *   The entity id to release the edit lock for.
 * @param $uid
 */
function content_lock_extnd_release($entity_type, $entity_id, $uid = NULL) {
  $query = db_delete('content_lock')
    ->condition('entity_type', $entity_type)
    ->condition('nid', $entity_id);
  if (!empty($uid))
    $query->condition('uid', $uid);
  $query->execute();
 // module_invoke_all('content_lock_extnd_released', $entity);
}

/**
 * Implement our own content_lock_extnd_entity_lockable().
 */
function content_lock_extnd_content_lock_extnd_entity_lockable($entity_type) {
  return array_filter(variable_get('content_lock_allowed_'. $entity_type . '_types', array()));
}

/**
 * Implement hook_entity_view() to inform user if he's holding locks on other entities.
 *
 * @param $entity
 *   The entity being viewed.
 * @param $view_mode
 *   The type of output being generated for the node.
 * @param $langcode
 *   The language code of the user.
 */
function content_lock_extnd_entity_view($entity, $entity_type, $view_mode, $langcode) {
  global $user;
  static $messages_shown = FALSE;
  if($entity_type == 'node') return;

  if ($view_mode != 'full'
      || !_content_lock_extnd_lockable_entity($entity->content['#entity_type'], $entity->content['#bundle'])) {
    return;
  }
  if (!$messages_shown) {
    _content_lock_extnd_show_warnings();
    $messages_shown = TRUE;
  }
  if (empty($entity->in_preview)) {
    /* Check if the user has pending locks and warn him. */
    content_lock_extnd_warn_pending_locks($user->uid);
  }
}

function _content_lock_extnd_show_warnings() {
  global $user;
  if(empty($_SESSION['content_lock_extnd'])) {
    return;
  }
  $data = unserialize($_SESSION['content_lock_extnd']);
  if(!is_array($data) || count($data) == 0) {
    return;
  }
  foreach($data as $entity_type => $messsage_id) {
  	foreach($messsage_id as $entity_id => $messsage) {
      if( _content_lock_extnd_still_locked($user->uid, $entity_type, $entity_id)) {
        drupal_set_message($messsage,'warning', FALSE);
      }
    }
  }
  $_SESSION['content_lock_extnd'] = '';
}

/**
 * For every lock a user current have on any nodes, print a warning messagt
 * with an link to release this node.
 *
 */
function content_lock_extnd_warn_pending_locks($uid) {
  static $warned_entities = array();
  static $content_lock_messages_printed = false;
  if($content_lock_messages_printed) {
    return;
  }
  if(array_key_exists($uid, $warned_entities)){
    // do nothing
  }
  else {
    // load form db
    $warned_entities[$uid] = array();
    $query = db_select('content_lock', 'c')
      ->fields('c', array('nid', 'entity_type'))
      ->condition('c.uid', $uid);
    foreach ($query->execute() as $lock) {
      $warned_entities[$uid][] = $lock;
    }
  }
  foreach($warned_entities[$uid] as $lock) {
  	$entity_info = entity_get_info($lock->entity_type);
		$destination = str_replace( '%' . $lock->entity_type, $lock->nid, $entity_info['default path']);
		if (empty($destination)) {
					$destination = $entity_info['admin ui']['path'] . '/view/' . $lock->nid;
		}
    $labels = db_select($entity_info['base table'], 'bt')
          ->fields('bt', array($entity_info['entity keys']['label']))
          ->condition($entity_info['entity keys']['id'], $lock->nid)
          ->execute()->fetchAssoc();
    $nodetitle_link = l(empty($labels) ? $lock->entity_type : array_pop($labels), $destination);
    $token = content_lock_extnd_get_release_token($lock->entity_type, $lock->nid);
    $releasethelock_link = l(t('release the lock'),"admin/content/$lock->entity_type/{$lock->nid}/content_lock/releaseown", array('query' => array('token' => $token)));
    _content_lock_extnd_save_lock_warning(t("The !node '!nodetitle_link' is locked by you. You may want to !releasethelock_link in order to allow others to edit.", array ('!node' => $lock->entity_type, '!nodetitle_link' => $nodetitle_link, '!releasethelock_link' => $releasethelock_link)), $lock->entity_type, $lock->nid);
  }
  $content_lock_messages_printed = true;
}

function _content_lock_extnd_save_lock_warning($message, $entity_type, $entity_id) {
  if(empty($_SESSION['content_lock_extnd'])) {
    $_SESSION['content_lock_extnd'] = '';
  }
  $data = unserialize($_SESSION['content_lock_extnd']);
  if(!is_array($data)) {
    $data = array();
  }
  if(array_key_exists($entity_type, $data) && array_key_exists($entity_id, $data)) {
    return;
  }
  $data[$entity_type][$entity_id] = $message;
  $_SESSION['content_lock_extnd'] = serialize($data);
}

/**
 * @return multitype:number
 */
function content_lock_extnd_views_api() {
  return array(
   'api' => 2.0,
   'path' => drupal_get_path('module', 'content_lock_extnd') . '/views',
  );
}

