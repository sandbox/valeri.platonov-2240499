<?php

/**
 * implements views api
 *
 */
function content_lock_extnd_views_data_alter(&$data) {

  // Our group in Views UI
//  $data['content_lock']['table']['group'] = t('Locked Content');
  $data['content_lock']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Locked Content'),
    'weight' => -10,
    'access query tag' => 'administer checked out documents',
    // need handler for default field
    'defaults' => array(
      'field' => 'entity_type',
    ),
  );

  // Relation ship to supported entity
  $entity_types_supported = variable_get('content_lock_extnd_entity_support', array());
  $entities_info = entity_get_info();
  foreach ($entities_info as $entity_type_name => $entity_type) {
    // $entity_type['label'] == 'node' configured already
    // $entity_type['label'] == 'user' configured already
  	if($entity_types_supported[$entity_type_name] == $entity_type['label'] ||
  	   $entity_types_supported[$entity_type_name] == 'Node' ||
  	   $entity_types_supported[$entity_type_name] == 'User' ) continue;

    $data['content_lock']['table']['join'][$entity_type['base table']] = array(
      'left_field' => $entity_type['entity keys']['id'],
      'field' => 'nid',
    );
    $data['content_lock']['nid_' . $entity_type['entity keys']['id']] = array(
    'title' => t('!entity_type locked', array('!entity_type' => $entity_type['label'])),
    'help' => t('The entity being locked'),
    'relationship' => array(
      'label' => t('!entity_type locked', array('!entity_type' => $entity_type['label'])),
      'base' => $entity_type['base table'],
      'base field' => $entity_type['entity keys']['id'],
      'skip base' => array($entity_type_name),
      ),
    );
  }

  $data['content_lock']['entity_type'] = array(
  	 // The item it appears as on the UI,
    'title' => t('Entity type'),
  	 // The help that appears on the UI,
    'help' => t('The content lock entity type.'),
     // Information for displaying a title as a field
    'field' => array(
    	// the real field. This could be left out since it is the same.
      'field' => 'entity_type',
    	// The group it appears in on the UI. Could be left out.
      'group' => t('Content lock'),
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
     // 'link_to_entity_type default' => TRUE,
     ),
/*
    'sort' => array(
      'handler' => 'views_handler_sort',
     ),

     // Information for accepting a title as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
 */
  );

  $data['content_lock']['bundle'] = array(
    'title' => t('Entity type Bundle'),
    'help' => t('The content lock entity type bundle.'),
    'field' => array(
      'field' => 'bundle',
      'group' => t('Content lock'),
      'handler' => 'views_handler_field_markup',
      'click sortable' => FALSE,
    ),
  );
  return $data;
}

?>