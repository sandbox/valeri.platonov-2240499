<?php

/**
 * Implementaion of hook_schema_alter($schema).
 *
 * will use nid as entity_id field for compatibility with content_lock module
 * but change primari index as (entity_type, node_id),
 * content_lock module use write_record() and autofill entity_type field with 'node' value
 */
function content_lock_extnd_schema_alter(&$schema) {
	if ( isset($schema['content_lock'])) {
    $schema['content_lock']['fields']['entity_type'] = array(
      'description' => 'Entity type.',
      'type' => 'varchar',
      'length' => 32,
      'not null' => TRUE,
      'default' => 'node',
    );
    $schema['content_lock']['fields']['bundle'] = array(
      'description' => 'Type of Entity type',
      'type' => 'varchar',
      'length' => 32,
      'not null' => TRUE,
      'default' => '',
    );
    /* used nid as entity_id for compatibility purpose
    $schema['content_lock']['fields']['entity_id'] = array(
      'description' => 'Identificator for entity',
      'type' => 'int',
      'size' => 'normal',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    );
    */
    //$schema['content_lock']['indexes']['entity_tib'] = array('entity_type','nid', 'bundle');
    $schema['content_lock']['primary key'] = array('nid', 'entity_type');
	}
  return $schema;
}

/**
 *  helper function with content_lock module compatibility
 */
function content_lock_extnd_allowed_clear($allowed_types = array()) {
	$allowed_var_name = array();
	$query = db_delete('variable')
      ->condition('name', 'content_lock_allowed_%_types', 'LIKE')
	    ->condition('name', 'content_lock_allowed_node_types', '!=')
	    ->condition('name', 'content_lock_allowed_formats', '!=');
	if($allowed_types) {
    foreach($allowed_types as $name => $label) {
  	  if($label) $allowed_var_name[] = 'content_lock_allowed_' . $name . '_types';
    }
		$query->condition('name', $allowed_var_name, 'NOT IN' );
	}
  $query->execute();
}

?>