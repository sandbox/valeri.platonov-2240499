<?php

/**
 * Providing an administration interface for content_lock_extnd.
 */
function content_lock_extnd_admin_settings(&$form = array()) {
	$form_alter = false;
	if($form) $form_alter = true;
  //get entity types
  $entity_types = array();
  $entity_types_supported = variable_get('content_lock_extnd_entity_support', array());
  $entities_info = entity_get_info();
  foreach ($entities_info as $entity_type_name => $entity_type) {
  	// $entity_type['label'] == 'node' configurable olways
  	if($entity_types_supported[$entity_type_name] == $entity_type['label']) continue;
    $entity_types[$entity_type_name] = $entity_type['label'];
    $form['content_lock_extnd_' . $entity_type['label'] ] = array(
     '#type' => 'fieldset',
     '#title' => t($entity_type['label']),
     '#description' => t('Configure "' . $entity_type['label'] . '" entity type'),
     '#collapsible' => TRUE,
     '#collapsed' => $entity_type_name == 'node' ? FALSE : TRUE,
  );
    $entity_bundle_types  = array();
    foreach($entity_type['bundles'] as $bundle_type_name => $bundle_type ) {
    	$entity_bundle_types[$bundle_type_name] = $bundle_type['label'];
    }
    $form['content_lock_extnd_' . $entity_type['label'] ]['content_lock_allowed_' . $entity_type_name . '_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Lockable entity types'),
      '#description' => t('Apply lock to the selected entity types. Leave blank for all entity types to be lockable.'),
      '#options' => $entity_bundle_types,
      '#default_value' => variable_get('content_lock_allowed_' . $entity_type_name . '_types', array()),
    );
  }
  if(!$form_alter)
  	return system_settings_form($form);
  else
  	return $form;
}

/**
 *  helper settings to exclude entity types from lockable settings
 */
function content_lock_extnd_admin_settings_exclude($form = array()) {
  //get entity types
  $entity_types = array();
  $entities_info = entity_get_info();
  foreach ($entities_info as $entity_type_name => $entity_type) {
    $entity_types[$entity_type_name] = $entity_type['label'] . ' (' . $entity_type['controller class'] . ')';
  }
  $form['content_lock_extnd_entity_support'] = array(
     '#type' => 'checkboxes',
     '#title' => t('Supported Lock entity types'),
     '#description' => t('Configure Supported Lockable entity types.'),
     '#options' => $entity_types,
     '#default_value' => array_replace(variable_get('content_lock_extnd_entity_support', array()), array('node' => 'node')),
  );
  $form['content_lock_extnd_entity_support']['node']['#disabled'] = module_exists('content_lock') ? TRUE : FALSE;
  $form['#submit'][] = 'content_lock_extnd_admin_settings_exclude_submit';
  return system_settings_form($form);
}

/**
 * clear settings for entity bundle if entity type not support
 */
function content_lock_extnd_admin_settings_exclude_submit($form_id, &$form_state) {
	if (module_exists('content_lock'))
		$form_state['values']['content_lock_extnd_entity_support']['node'] = 'node';
  content_lock_extnd_allowed_clear($form_state['input']['content_lock_extnd_entity_support']);
}

?>