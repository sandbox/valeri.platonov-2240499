<?php
/**
/* Implements hook_views_pre_view().
*/
function content_lock_extnd_views_pre_view(&$view, &$display_id, &$args) {
  global $user;
  if ($view->name =='locked_content') {

// dpm($view->display['page']);

    if ($display_id == 'page') {
      $type = $args[0];
      $view_filters = $view->display_handler->get_option('filters');
      $view_fields = $view->display_handler->get_option('fields');

        if (isset($view_filters['status'])) {
          unset($view_filters['status']);
        }
        if (isset($view_fields['status'])) {
          unset($view_fields['status']);
        }

      $view->display_handler->override_option('filters', $view_filters);
      $view->display_handler->override_option('fields', $view_fields);
    }
  }
}

/**
 * Implements hook_views_query_alter().
 */
function content_lock_extnd_views_query_alter(&$view, &$query) {
  if ($view->name == 'locked_content') { // && isset($query->where[1]['conditions']) && $node = menu_get_object()) {
  /*
  	$query->where[1]['conditions'][] = array(
      'field' => 'node.nid',
      'operator' => '<>',
      'value' => $node->nid,
    );
  */
  }
}

?>

